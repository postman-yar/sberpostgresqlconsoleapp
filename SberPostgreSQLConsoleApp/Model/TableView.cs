﻿namespace SberPostgreSQLConsoleApp.Model
{
    public class TableView
    {
        public string Id { get; }
        public string TableName { get; }
        public string TableFullName { get; }

        public TableView(string id, string tableName, string tableFullName)
        {
            this.Id = id;
            this.TableName = tableName;
            this.TableFullName = tableFullName;
        }
    }
}
