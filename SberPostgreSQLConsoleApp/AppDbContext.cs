﻿using Microsoft.EntityFrameworkCore;
using SberPostgreSQLConsoleApp.Entity;

namespace SberPostgreSQLConsoleApp
{
    public class AppDbContext : DbContext
    {
        public AppDbContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=ec2-54-194-147-61.eu-west-1.compute.amazonaws.com;Port=5432;Database=d73cbmg5ao8e44;Username=ucabnopxdutayb;Password=79179f778ec9419d1a6f125ed600a124c2306acf3c20552f774c3524254759a2;SSLMode=Prefer;Trust Server Certificate=true");
        }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Operation> Operations { get; set; }

    }
}
