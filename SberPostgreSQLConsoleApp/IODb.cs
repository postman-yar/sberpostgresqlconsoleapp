﻿using SberPostgreSQLConsoleApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SberPostgreSQLConsoleApp
{
    public class IODb
    {
        private AppDbContext Context { get; }
        public List<TableView> TableList { get; }

        public IODb(AppDbContext context)
        {
            this.Context = context;
            this.TableList = GetTables();
        }

        private List<TableView> GetTables()
        {
            var tables = Context.Model.GetEntityTypes().ToList();

            var tList = new List<TableView>();

            var id = 1;
            foreach (var t in tables)
            {
                tList.Add(new TableView(id++.ToString(), t.ClrType.Name, t.ClrType.FullName));
            }

            return tList;
        }

        public void AddToTable(string tableId)
        {
            var t = TableList.FirstOrDefault(x => x.Id == tableId);
            var handle = Activator.CreateInstance(Assembly.GetCallingAssembly().FullName, t.TableFullName);
            var obj = handle.Unwrap();

            foreach (var field in obj.GetType().GetProperties())
            {
                if (IsFieldCorrect(field))
                {
                    Console.WriteLine($"Введите значение поля {field.Name}:");
                    var value = Console.ReadLine();
                    if (field.PropertyType == typeof(int))
                        field.SetValue(obj, int.Parse(value));
                    if (field.PropertyType == typeof(double))
                        field.SetValue(obj, double.Parse(value.Replace(".",",")));
                    if (field.PropertyType == typeof(string))
                        field.SetValue(obj, value);
                    if (field.PropertyType == typeof(DateTime))
                    {
                        DateTime.TryParse(value, out DateTime result);
                        field.SetValue(obj, result);
                    }
                }
            }
            Context.Add(obj);
            Context.SaveChanges();
        }

        private bool IsFieldCorrect(PropertyInfo field)
        {
            if (field.Name.ToLower() != "id" && (field.PropertyType == typeof(int) ||
                                                 field.PropertyType == typeof(string) ||
                                                 field.PropertyType == typeof(DateTime) ||
                                                 field.PropertyType == typeof(double)))
                return true;
            return false;
        }
    }
}