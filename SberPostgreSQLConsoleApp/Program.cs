﻿using SberPostgreSQLConsoleApp.Entity;
using System;
using System.Linq;

namespace SberPostgreSQLConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            AppDbContext Context = new AppDbContext();
            while (true)
            {
                Console.WriteLine("");
                Console.WriteLine("Выберите действие:");
                Console.WriteLine(">1. Вывести данные");
                Console.WriteLine(">2. Добавить данные");
                Console.WriteLine(">0. Выход");

                var action = Console.ReadLine();
                switch (action)
                {
                    case "0":
                        return;
                    case "1":
                        OutputData(Context);
                        break;
                    case "2":
                        WriteData(Context);
                        break;
                    default:
                        break;
                }
            }
        }

        public static void WriteData(AppDbContext context)
        {
            var ioDb = new IODb(context);
            var tableList = ioDb.TableList;

            Console.WriteLine("Выберите таблицу:");
            foreach (var t in tableList)
            {
                Console.WriteLine($">{t.Id}. {t.TableName}");
            }

            Console.WriteLine(">0. Выход");

            var action = Console.ReadLine();
            switch (action)
            {
                case "0":
                    return;
                default:
                    ioDb.AddToTable(action);
                    break;
            }

        }

        public static void OutputData(AppDbContext context)
        {
            var clients = context.Clients.ToList();
            Console.WriteLine("Clients list:");
            foreach (var u in clients)
            {
                Console.WriteLine(u);
            }
            Console.WriteLine("");

            var accounts = context.Accounts.ToList();
            Console.WriteLine("Accounts list:");
            foreach (var u in accounts)
            {
                Console.WriteLine(u);
            }
            Console.WriteLine("");

            var operations = context.Operations.ToList();
            Console.WriteLine("Operations list:");
            foreach (var u in operations)
            {
                Console.WriteLine(u);
            }
            Console.WriteLine("");
        }
    }
}
