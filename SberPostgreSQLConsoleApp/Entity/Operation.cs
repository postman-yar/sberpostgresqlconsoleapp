﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql.EntityFrameworkCore.PostgreSQL.Storage.Internal.Mapping;

namespace SberPostgreSQLConsoleApp.Entity
{
    public class Operation
    {
        public int Id { get; set; }
        public DateTime Date { get; set; } 
        public int AccountId { get; set; }
        public Account Account { get; set; }
        public double Sum { get; set; }

        public override string ToString()
        {
            return $"{Id} - {Date} - {Account.Client} - {Sum}";
        }
    }
}
