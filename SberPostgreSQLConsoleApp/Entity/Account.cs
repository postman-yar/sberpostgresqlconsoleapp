﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SberPostgreSQLConsoleApp.Entity
{
    public class Account
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public Client Client { get; set; }
        public int Rest { get; set; }

        public override string ToString()
        {
            return $"{Id} - {Client} - {Rest}";
        }
    }
}
