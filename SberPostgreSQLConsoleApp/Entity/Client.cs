﻿namespace SberPostgreSQLConsoleApp.Entity
{
    public class Client
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public override string ToString()
        {
            return $"{FirstName} {SecondName}";
        }
    }
}